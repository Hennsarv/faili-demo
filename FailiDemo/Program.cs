﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace FailiDemo
{


    static class Program
    {
        static string FailiNimi = @"..\..\nimekiri.txt";
        public static void Main()
        {
            

            var Nimekiri = 
            File.ReadAllLines(FailiNimi)
               .Select(x => new Inimene(x))
               
               .ToList()
               ;
            foreach (var x in Nimekiri)
            {
                Console.WriteLine(x);
            }
        }

        public static IEnumerable<string> Write(this IEnumerable<string> read, string kuhu)
        {
            File.WriteAllLines(kuhu, read);
            return read;
        }

        public static int ToInt(this string x)
        {
            int i = -1;
            int.TryParse(x, out i);
            return i;
            // teisendab teksti arvuks, kui ei teisendu, siis -1 ks
        }

        
    }

    class Inimene
    {
        string _Nimi;
        int _Vanus;

        public string Nimi
        {
            get => _Nimi;
            set {
                _Nimi = String.Join(" ",
                        value.Split(' ')
                            .Select(x => 
                                    x.Substring(0, 1).ToUpper() + 
                                    x.Substring(1).ToLower() 
                                    )
                                )
                    ;
                    }
        }

        public int Vanus { get => _Vanus; set => _Vanus = value > 0 ? value : _Vanus; }
        public Inimene(string andmed)
        {
            Nimi = andmed.Split(',')[0];
            int.TryParse(andmed.Split(',')[1], out _Vanus);
        }

        public override string ToString()
        {
            return $"Inimene {Nimi} vanusega {Vanus}";
        }
    }
}
